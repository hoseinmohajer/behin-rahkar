import React from "react";
import { CheckboxWrapper } from "./style";

const CheckBox = (props) => {
  return (
    <CheckboxWrapper
      checked={props.checked}
      disabled={props.disabled}
      bgColor={props.bgColor}
      color={props.color}
      onClick={() => {
        if (!props.disabled) {
          props.onSelect && props.onSelect();
        }
      }}
    >
      {props.label || props.children}
    </CheckboxWrapper>
  );
};

export default CheckBox;
