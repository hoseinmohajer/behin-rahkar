import styled from "styled-components";
import Colors from "../../constants/colors";

export const CheckboxWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  color: ${Colors.black};
  font-size: 14px;
  padding-left: 29px;
  position: relative;
  height: 21px;
  cursor: pointer;
  &:before {
    content: " ";
    overflow: hidden;
    position: absolute;
    left: 0;
    top: 0;
    width: 18px;
    height: 18px;
    border-radius: 3px;
    background-color: ${(props) => props.bgColor};
    border: 2px solid ${Colors.darkGray};
  }
  ${(props) => {
    if (props.checked) {
      return `
				&:after {
					content: ' ';
					position: absolute;
					left: 0;
					top: 0;
					width: 18px;
					height: 18px;
					border-radius: 3px;
					background-color: ${props.color};
					border: 2px solid ${props.color};
				}
			`;
    }
  }}
  ${(props) => {
    if (props.disabled) {
      return `
      &:before {
        border-color: #4a5568;
        background-color: #4a5568;
        opacity: 0.5;
        cursor: default;
      }
      `;
    }
  }}
	@media (min-width: 1280px) {
    font-size: 16px;
  }
`;
