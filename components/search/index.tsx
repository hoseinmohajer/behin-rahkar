import React from "react";
import { Container, Icon } from "./style";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

const Index = () => {
  return (
    <Container>
      <Icon>
        <FontAwesomeIcon icon={faSearch} />
      </Icon>
      <input type="text" placeholder="Search the product you are looking for..." />
    </Container>
  );
};

export default Index;
