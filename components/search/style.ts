import styled from "styled-components";
import Colors from "../../constants/colors";

export const Container = styled.div`
  border-radius: 99px;
  background-color: ${Colors.lightGray};
  margin-top: 40px;
  margin-bottom: 24px;
  padding: 13px 26px 13px 67px;
  position: relative;
  height: 48px;
  font-size: 14px;
  color: ${Colors.gray50};
  width: calc(100% - 34px);
  input {
    width: 100%;
    border: 0;
    outline: none !important;
    background-color: ${Colors.lightGray};
    height: 25px;
    &:hover,
    &:active,
    &:visited,
    &:current,
    &:after,
    &:before,
    &:focus {
      border: 0;
      outline: none !important;
    }
  }
`;

export const Icon = styled.div`
  position: absolute;
  height: 18px;
  left: 26px;
  top: calc(50% - 10px);
  color: ${Colors.gray100};
  font-size: 18px;
`;
