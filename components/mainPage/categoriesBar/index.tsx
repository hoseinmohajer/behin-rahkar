import React from "react";
import Image from "next/image";

// todo: should be complete; instead of image we have to have a category menu
const CategoryBar: React.FunctionComponent = () => {
  return <Image width={1600} height={64} layout="responsive" src="/images/topNav.png" alt="Avatar" />;
};

export default CategoryBar;
