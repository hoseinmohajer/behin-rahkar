import React from "react";
import { Container } from "./style";
import CategoryBar from "./categoriesBar";
import Sidebar from "./sidebar";
import ProductsContainer from "./productsContainer";

const Index = () => {
  return (
    <>
      <CategoryBar />
      <Container>
        <Sidebar />
        <ProductsContainer />
      </Container>
    </>
  );
};

export default Index;
