import styled from "styled-components";
import Colors from "../../../constants/colors";

export const Container = styled.div`
  width: calc(100% - 334px);
  min-width: calc(100% - 320px);
  border-radius: 8px;
  box-shadow: 0 1px 2px rgba(58, 58, 68, 0.24), 0 2px 4px rgba(90, 91, 106, 0.24);
  min-height: 500px;
  padding: 14px 0 14px 14px;
`;
export const Content = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  flex-direction: row;
  flex-wrap: wrap;
`;
export const ButtonContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 40px;
`;
export const Right = styled.div`
  width: 40%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
export const Left = styled.div`
  width: 45%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  .left-image {
    margin-top: 0px !important;
    margin-bottom: 1px !important;
  }
  .left-image2 {
    margin-bottom: 4px !important;
  }
`;
