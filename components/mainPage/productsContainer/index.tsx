import React, { useEffect, useState } from "react";
import { Container, Content, ButtonContainer, Right, Left } from "./style";
import Card from "../../card";
import { connect } from "react-redux";
import { productsSlice } from "../../../redux/slices/products";
import { useRouter } from "next/router";
import Search from "../../search";
import Image from "next/image";

interface DataInterface {
  id: number;
  title: string;
  price: number;
  description: string;
  category: string;
  image: string;
  rating: {
    rate: number;
    count: number;
  };
}

const Index = ({ getProducts, products }: { getProducts: Function; products: { data: DataInterface[] } }) => {
  const router = useRouter();
  const [data, setData] = useState<DataInterface[]>([]);
  useEffect(() => {
    getProducts();
  }, [router.query]);
  useEffect(() => {
    const slug = router.query;
    const categories = slug?.categories?.toString().split("-");
    let filteredData = products.data;
    if (categories) {
      filteredData = products.data.filter((item: { category: string }) => categories.indexOf(item.category) !== -1);
    }
    setData(filteredData);
  }, [products]);
  return (
    <Container>
      <ButtonContainer>
        <Left>
          <Image className={"left-image2"} src="/images/Select-2.svg" width={136} height={36} alt="button" />
          <Image className={"left-image"} src="/images/Select-1.svg" width={112} height={30} alt="button" />
          <Image className={"left-image"} src="/images/Select.svg" width={152} height={30} alt="button" />
        </Left>
        <Right>
          <Image src="/images/Segmented3.svg" width={220} height={36} alt="button" />
          <Image src="/images/Segmented2.svg" width={136} height={36} alt="button" />
        </Right>
      </ButtonContainer>
      <Search />
      <Content>{data && data.map((item, key) => <Card data={item} key={key} />)}</Content>
    </Container>
  );
};

Index.getInitialProps = async ({ store }: { store: any }) => {
  const data = await store.dispatch(productsSlice.actions.getProducts());
  return {};
};

const mapStateToProps = (state: any) => {
  return {
    products: state.products.products,
  };
};

const mapDispatchToProps = (dispatch: any) => ({
  getProducts: () => dispatch(productsSlice.actions.getProducts()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Index);
