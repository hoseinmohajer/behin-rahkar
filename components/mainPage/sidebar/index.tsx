import React, { useEffect, useState } from "react";
import { Container, Title, ClearFilter, Header, Tags, DropDownContainer } from "./style";
import Tag from "./tag";
import DropDown from "../../../components/dropDown";
import queryString from "query-string";
import { useRouter } from "next/router";
import isEmpty from "../../../helpers/isEmpty";

interface CategoriesInterface {
  checked: boolean;
  name: string;
}
const Index = () => {
  const router = useRouter();
  const [categories, setCategories] = useState<CategoriesInterface[]>([]);
  const [tags, setTags] = useState<any>({});

  useEffect(() => {
    const initialCat = [
      {
        name: "electronics",
        checked: false,
      },
      {
        name: "jewelery",
        checked: false,
      },
      {
        name: "men",
        checked: false,
      },
      {
        name: "women",
        checked: false,
      },
    ];
    const parsed = queryString.parse(location.search);
    let parsedArray = Object.values(parsed).toString().split("-");
    if (!isEmpty(parsed)) {
      const cats = initialCat.map((item) => {
        if (parsedArray.indexOf(item.name) !== -1) {
          item.checked = true;
        }
        return item;
      });
      setCategories(cats);
    } else {
      setCategories(initialCat);
    }
  }, [router.query]);

  useEffect(() => {
    const parsed = queryString.parse(location.search);
    if (!isEmpty(router.query)) {
      setTags({ ...tags, ...parsed });
    } else {
      setTags({});
    }
  }, [router.query]);

  const changeHandler = async (checked: boolean, name: string) => {
    const parsed = await queryString.parse(location.search);
    let parsedArray = Object.values(parsed).toString().split("-");
    // when we dont have query string
    if (isEmpty(parsed)) {
      parsedArray = [name];
    } else {
      if (parsedArray.indexOf(name) !== -1) {
        parsedArray = parsedArray.filter((item) => item !== name);
      } else {
        parsedArray = [...parsedArray, ...[name]];
      }
    }
    const normalized = categories.map((item) => {
      if (item.name === name) {
        return { ...item, checked: !checked };
      } else {
        return item;
      }
    });
    setCategories(normalized);
    if (checked) {
      parsed.categories = name;
    } else {
      parsed.categories = "";
    }
    const stringified = await queryString.stringify({
      [Object.keys({ categories: router.query })[0]]: parsedArray.toString().split(",").join("-"),
    });
    if (parsedArray.toString().split(",").join("-")) {
      await router.push({ pathname: router.pathname, query: stringified }, undefined, { shallow: true });
    } else {
      await router.replace("/", undefined, { shallow: true });
    }
  };

  const clickHandler = async (name: string | null) => {
    const parsed = await queryString.parse(location.search);
    const parsedArray = Object.values(parsed).toString().split("-");
    const newQuery = parsedArray.filter((item) => item !== name);
    const stringified = await queryString.stringify({
      [Object.keys({ categories: router.query })[0]]: newQuery.toString().split(",").join("-"),
    });
    if (newQuery.toString().split(",").join("-")) {
      await router.push({ pathname: router.pathname, query: stringified }, undefined, { shallow: true });
    } else {
      await router.replace("/", undefined, { shallow: true });
    }
  };
  return (
    <Container>
      <Header>
        <Title>Filter By</Title>
        <ClearFilter>Clear Filters</ClearFilter>
      </Header>
      <Tags>
        {tags &&
          Object.values(tags)
            .toString()
            .split("-")
            .filter((item) => item !== "")
            .map((item, key: number) => (
              <Tag onClick={() => clickHandler(item)} key={key}>
                {item}
              </Tag>
            ))}
      </Tags>
      <DropDownContainer>
        <DropDown data={categories} title="categories" changeHandler={changeHandler} />
      </DropDownContainer>
    </Container>
  );
};

export default Index;
