import React from "react";
import { Tag, Close } from "./style";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

const Index = ({ onClick, children }: { onClick: () => any; children: string }) => {
  return (
    <Tag>
      {children}
      <Close onClick={() => onClick()}>
        <FontAwesomeIcon icon={faTimes} />
      </Close>
    </Tag>
  );
};

export default Index;
