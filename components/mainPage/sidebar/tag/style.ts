import styled from "styled-components";
import Colors from "../../../../constants/colors";

export const Tag = styled.div`
  min-width: 50px;
  height: 32px;
  padding: 6px 32px 6px 12px;
  font-size: 14px;
  position: relative;
  color: ${Colors.gray};
  background-color: ${Colors.lightGray};
  border-radius: 99px;
  margin-left: 8px;
  margin-top: 12px;
`;
export const Close = styled.div`
  width: 20px;
  height: 20px;
  border-radius: 50%;
  font-size: 14px;
  color: ${Colors.blue};
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  right: 4px;
  top: calc(50% - 10px);
  padding: 4px;
  cursor: pointer;
  &:hover {
    background-color: ${Colors.white};
  }
`;
