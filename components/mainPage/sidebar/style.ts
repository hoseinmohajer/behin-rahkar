import styled from "styled-components";
import Colors from "../../../constants/colors";

export const Container = styled.div`
  min-width: 300px;
  min-height: 500px;
  margin-right: 20px;
`;
export const Title = styled.div`
  font-size: 24px;
  font-weight: 500;
  color: ${Colors.black};
  text-transform: capitalize;
`;
export const ClearFilter = styled.div`
  font-size: 20px;
  font-weight: 500;
  color: ${Colors.blue};
  cursor: pointer;
`;
export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
export const Tags = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  flex-wrap: wrap;
  margin-top: 24px;
  width: 100%;
`;
export const DropDownContainer = styled.div`
  padding: 12px 8px;
`;
