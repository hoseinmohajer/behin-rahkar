import React, { ReactElement } from "react";
import { Container, GlobalStyle, InnerContainer } from "./style";
import Head from "next/head";
import Footer from "./footer";
import TopBar from "./topBar";

const MainLayout: any = ({ children }: { children: ReactElement }) => {
  return (
    <>
      <GlobalStyle />
      <Head>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="viewport" content="initial-scale=1, maximum-scale=1" />
        <title>HoseinMohajer - BehinRahkar - TEST</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="preload" href="/fonts/Inter/Inter-VariableFont_slnt,wght.ttf" as="font" crossOrigin="" />
        <link rel="preload" href="/fonts/Quicksand/Quicksand-VariableFont_wght.ttf" as="font" crossOrigin="" />
      </Head>
      <Container>
        <TopBar />
        <InnerContainer>{children}</InnerContainer>
      </Container>
    </>
  );
};

export default MainLayout;
