import styled, { createGlobalStyle } from "styled-components";
import Colors from "../../constants/colors";

export const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: "Inter";
    src: url("/fonts/Inter/Inter-VariableFont_slnt,wght.ttf");
    font-style: normal;
    font-weight: normal;
    font-display: swap;
  }
  @font-face {
    font-family: "Quicksand";
    src: url("/fonts/Quicksand/Quicksand-VariableFont_wght.ttf");
    font-style: normal;
    font-weight: bold;
    font-display: swap;
  }
  * {
    box-sizing: border-box;
    font-family: 'Inter', sans-serif;
  }
  body {
    padding: 54px 47px;
    margin: 0;
    background-color: ${Colors.lightGray};
  }
`;

export const Container = styled.div`
  width: 100%;
  max-width: 1600px;
  margin: 0 auto;
  min-height: 100vh;
  padding: 24px 16px;
  border-radius: 20px;
  background-color: ${Colors.white};
`;

export const InnerContainer = styled.div`
  width: calc(100% - 400px);
  margin: 44px auto 0 auto;
`;
