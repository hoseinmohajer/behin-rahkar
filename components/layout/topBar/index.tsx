import React from "react";
import {
  Container,
  RightSide,
  LeftSide,
  Logo,
  SellButton,
  RegisterButton,
  Avatar,
  LogoText,
  ButtonContainer,
  Consumer,
} from "./style";
import Image from "next/image";
import Button from "../../button";

const TopBar = () => {
  return (
    <Container>
      <RightSide>
        <Logo>s</Logo>
        <LogoText>Shopka</LogoText>
        <SellButton>Sell on Shopka</SellButton>
        <RegisterButton>Register</RegisterButton>
      </RightSide>
      <LeftSide>
        <Consumer>Consumer Electronics</Consumer>
        <ButtonContainer>
          <Button onClick={() => alert("login")} size="md" type="primary">
            login
          </Button>
          <Button onClick={() => alert("card")} size="md" type="primary">
            Card
          </Button>
        </ButtonContainer>
        <Avatar>
          <Image src="/images/avatar.svg" alt="Avatar" width={40} height={40} layout="intrinsic" />
        </Avatar>
      </LeftSide>
    </Container>
  );
};

export default TopBar;
