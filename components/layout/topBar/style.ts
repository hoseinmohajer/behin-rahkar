import styled from "styled-components";
import Colors from "../../../constants/colors";

export const Container = styled.header`
  width: 100%;
  max-width: 1600px;
  margin: 0 auto;
  min-height: 40px;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
`;
export const RightSide = styled.div`
  width: 50%;
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
`;
export const LeftSide = styled.div`
  width: 50%;
  display: flex;
  justify-content: flex-end;
  align-items: flex-start;
`;
export const Logo = styled.div`
  min-width: 40px;
  min-height: 40px;
  border-radius: 12px;
  background: url("/images/logo.svg") no-repeat center center;
  display: flex;
  justify-content: center;
  align-items: center;
  text-transform: uppercase;
  font-size: 32px;
  color: ${Colors.white};
  font-weight: bold;
  margin-right: 12px;
`;
export const LogoText = styled.div`
  font-size: 36px;
  text-transform: capitalize;
  color: ${Colors.black};
`;
export const SellButton = styled.div`
  color: ${Colors.gray};
  font-size: 20px;
  cursor: pointer;
  margin-left: 33px;
  min-height: 40px;
  padding: 8px;
`;
export const RegisterButton = styled.div`
  color: ${Colors.gray};
  font-size: 20px;
  cursor: pointer;
  margin-left: 33px;
  min-height: 40px;
  padding: 8px;
`;
export const Avatar = styled.div`
  margin-left: 16px;
`;
export const ButtonContainer = styled.div`
  width: 230px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
export const Consumer = styled.div`
  margin-right: 123px;
  color: ${Colors.blue};
  font-size: 20px;
  text-transform: capitalize;
  min-height: 40px;
  padding: 8px;
`;
