import React from "react";
import { useRouter } from "next/router";
import Link from "next/link";

const Footer = () => {
  const router = useRouter();
  return (
    <footer>
      <p>
        © 2021 Behin-Rahkar test project &nbsp;&nbsp;
        <small>
          <Link passHref={true} href="https://github.com/hoseinmohajer">
            <a href="#" target="_blank">
              Hosein Mohajer
            </a>
          </Link>
        </small>
      </p>
    </footer>
  );
};

export default Footer;
