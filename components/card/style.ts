import styled from "styled-components";
import Colors from "../../constants/colors";

export const Container = styled.div`
  box-sizing: border-box;
  width: 260px;
  min-height: 472px;
  padding: 14px;
  margin-right: 14px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: column;
  &:hover {
    box-shadow: 0 2px 4px rgba(59, 69, 123, 0.2), 0 4px 8px rgba(92, 107, 192, 0.2);
    border-radius: 8px;
  }
`;
export const ImageContainer = styled.div`
  // code ...
  width: 227px;
  height: 224px;
  margin-bottom: 12px;
  img {
    width: 227px;
    height: 224px;
  }
`;
export const Description = styled.div`
  width: 227px;
  margin-bottom: 8px;
  font-size: 16px;
  color: ${Colors.black};
  font-weight: normal;
  text-align: left;
  height: 100px;
  overflow: hidden;
`;
export const Price = styled.div`
  font-size: 24px;
  font-weight: bold;
  color: ${Colors.black};
  text-align: left;
  width: 100%;
`;
export const Note = styled.div`
  width: 227px;
  margin-bottom: 8px;
  font-size: 14px;
  color: ${Colors.gray};
  font-weight: normal;
  text-align: left;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;
export const Footer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`;
export const Rating = styled.div`
  color: orange;
`;
