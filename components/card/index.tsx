import React from "react";
import { Container, ImageContainer, Description, Price, Note, Footer, Rating } from "./style";
import Button from "../button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import Image from "next/image";

const Index = ({ data }: { data: any }) => {
  const imageLoader = ({ src, width, quality }: { src: string; width: string; quality: string } | any) => {
    return `${src}?w=${width}&q=${quality || 75}`;
  };

  if (data) {
    return (
      <Container>
        <ImageContainer>
          <Image loader={imageLoader} src={data.image} layout="responsive" width={227} height={224} alt={data.title} />
        </ImageContainer>
        <Description>{data.description}</Description>
        <Price>{data.price}</Price>
        <Note>{data.title}</Note>
        <Footer>
          <Rating>
            {data?.rating?.rate} &nbsp; <span>{data?.rating?.count}</span>
          </Rating>
          <Button type="primary" size="md" onClick={() => alert("save it ...")}>
            <div>
              <FontAwesomeIcon icon={faHeart} />
              <span style={{ marginLeft: "12px" }}>Save</span>
            </div>
          </Button>
        </Footer>
      </Container>
    );
  } else {
    return <div>Loading ...</div>;
  }
};

export default Index;
