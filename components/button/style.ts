import styled from "styled-components";
import Colors from "../../constants/colors";
// @ts-ignore

interface TypeInterface {
  type: string;
}
interface DisableInterface {
  isDisabled?: boolean;
}
interface SizeInterface {
  size: string;
}
// @ts-ignore: Unreachable code error
interface ButtonWrapperInterface {
  isDisabled?: boolean;
  type: string;
  size: string;
}
export const ButtonWrapper = styled.div<ButtonWrapperInterface>`
  display: flex;
  justify-content: center;
  align-items: center;
  font-family: "Quicksand", sans-serif;
  ${({ type }: TypeInterface) => {
    switch (type) {
      case "primary":
        return `
				color: ${Colors.blue};
				border-radius: 4px;
				background-color: ${Colors.white};
        border: 1px solid ${Colors.lightBlue};
				&:hover {
				  box-shadow: 0px 2px 4px rgba(27, 78, 163, 0.2), 0px 4px 8px rgba(41, 121, 255, 0.2);
				  border: 1px solid ${Colors.white};
				}
			`;
      case "secondary":
        return `
				background-color: #ffffff;
				color: #101010;
				&:hover {
					background-color: #000;
					color: #fff;
				}
			`;
    }
  }};
  ${({ isDisabled }: DisableInterface) => {
    return `
			opacity: ${isDisabled ? 0.5 : 1};
			cursor: ${isDisabled ? "not-allowed" : "pointer"}
		`;
  }};
  ${({ size }: SizeInterface) => {
    switch (size) {
      case "sm":
        return `
					padding: 4px 12px;
					font-size: 14px;
					font-weight: normal;
					min-width: 70px;
					min-height: 20px;
				`;
      case "md":
        return `
					padding: 8px 12px;
					font-size: 16px;
					font-weight: normal;
					min-width: 104px;
					min-height: 40px;
					@media (min-width: 1024px) {
						font-size:18px;
					}
				`;
      case "sm-wide":
        return `
					padding: 8px 23px;
					font-size: 20px;
					font-weight: normal;
					min-width: 185px;
					min-height: 30px;
				`;
    }
  }};
`;
