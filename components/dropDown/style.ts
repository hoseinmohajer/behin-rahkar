import styled from "styled-components";
import Colors from "../../constants/colors";

export const Container = styled.div`
  background-color: ${Colors.lightestGray};
  width: 100%;
  min-height: 32px;
  border-radius: 4px;
  margin-top: 12px;
`;
export const Header = styled.div`
  width: 100%;
  padding: 8px 12px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  -moz-user-select: none;
  -khtml-user-select: none;
  -webkit-user-select: none;
  -ms-user-select: none;
  user-select: none;
`;
export const Title = styled.div`
  font-size: 16px;
  font-weight: 500;
  text-transform: capitalize;
`;
export const Icon = styled.i`
  color: ${Colors.darkGray};
  border-radius: 50%;
  width: 20px;
  height: 20px;
  &:hover {
    background-color: ${Colors.lightGray};
  }
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`;
export const Body = styled.div`
  padding: 19px;
  display: ${(props: { show: boolean }) => (props.show ? "flex" : "none")};
  justify-content: flex-start;
  align-items: flex-start;
  flex-direction: column;
  -moz-user-select: none;
  -khtml-user-select: none;
  -webkit-user-select: none;
  -ms-user-select: none;
  user-select: none;
`;
export const Item = styled.div`
  margin-bottom: 10px;
`;
