import React, { useState } from "react";
import { Container, Header, Title, Icon, Body, Item } from "./style";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretUp, faCaretDown } from "@fortawesome/free-solid-svg-icons";
import CheckBox from "../checkbox";
import Colors from "../../constants/colors";
import { useRouter } from "next/router";

interface DataInterface {
  name: string;
  checked: boolean;
}
const Index = ({ data, title, changeHandler }: { data: DataInterface[]; title: string; changeHandler: Function }) => {
  const router = useRouter();
  const [show, toggleMenu] = useState(true);

  return (
    <Container>
      <Header>
        <Title>{title}</Title>
        <Icon onClick={() => toggleMenu(!show)}>
          <FontAwesomeIcon icon={show ? faCaretUp : faCaretDown} />
        </Icon>
      </Header>
      <Body show={show}>
        {data.map((item: DataInterface, key: number) => {
          const slug = router.query.categories;
          const slugArray = slug ? slug.toString().split("-") : [];
          let newSlug: string[] | string = [];
          if (slugArray.length === 0) {
            newSlug = [item.name];
          } else if (slugArray && slugArray.length !== 0 && !slugArray.find((slug) => slug === item.name)) {
            newSlug = [...slugArray, ...[item.name]];
          } else {
            newSlug = slugArray ? slugArray.filter((slug) => slug !== item.name) : [];
          }
          newSlug = newSlug.toString().split(",").join("-");
          return (
            <Item key={key}>
              <CheckBox
                bgColor={Colors.white}
                color={Colors.blue}
                label={item.name}
                onSelect={() => {
                  changeHandler(item.checked, item.name);
                }}
                checked={item.checked}
              />
            </Item>
          );
        })}
      </Body>
    </Container>
  );
};

export default Index;
