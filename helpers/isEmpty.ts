const isEmpty = function isEmpty(obj: {}) {
  for (let key in obj) {
    if (Object.keys(obj).indexOf(key) !== -1) return false;
  }
  return true;
};

export default isEmpty;
