// jest.config.ts
import type {Config} from '@jest/types';
const {defaults} = require('jest-config');

export default async (): Promise<Config.InitialOptions> => {
  return {
    // verbose: true,
    moduleFileExtensions: [...defaults.moduleFileExtensions, 'ts', 'tsx'],
    testPathIgnorePatterns: ['<rootDir>/next','<rootDir>/node_modules'],
    setupFilesAfterEnv: ['<rootDir>/setupTests.js'],
    moduleNameMapper: {
      '/^.+.(css|less|scss|sass)$/': 'identity-obj-proxy'
    },
    // transform: {
    //   '/^.+.(js|jsx|ts|tsx)$/': '<rootDir>/node_modules/babel-jest',
    //   '/^.+.(css|less|scss|sass)$/': 'identity-obj-proxy',
    // }
  };
};
