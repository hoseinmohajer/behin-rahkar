/**
 * @jest-environment jsdom
 */
import ShallowRenderer from "react-test-renderer/shallow";
const renderer = new ShallowRenderer();

import Button from "../components/button/index";

describe("Button Component.", () => {
  it("test custom button component", () => {
    const onClick = jest.fn();
    const testComp = renderer.render(
      <Button type="primary" size="md" onClick={onClick}>
        test
      </Button>
    );
    const result = renderer.getRenderOutput();
    expect(result.props.children).toEqual("test");
  });
});
