export default {
  white: "#fff",
  blue: "#2264D1",
  lightBlue: "#9DC2FF",
  gray: "#5A5B6A",
  gray50: "#7C7C8A",
  gray100: "#787885",
  lightestGray: "#F7F7FA",
  lightGray: "#E5E5E5",
  darkGray: "#4A4B57",
  black: "#000",
};
