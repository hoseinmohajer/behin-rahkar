import React from "react";
import MainLayout from "../components/layout";
import MainPage from "../components/mainPage";

export default function Home() {
  return (
    <MainLayout>
      <MainPage />
    </MainLayout>
  );
}
