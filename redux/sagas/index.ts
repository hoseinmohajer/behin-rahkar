import { all } from "redux-saga/effects";

import { watcherProducts } from "./products";

export default function* rootSaga() {
  yield all([watcherProducts()]);
}
