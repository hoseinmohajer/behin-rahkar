import { takeLatest, call, put } from "redux-saga/effects";
import { productsSlice } from "../slices/products";
import axios from "axios";

export function* watcherProducts() {
  yield takeLatest(productsSlice.actions.getProducts.type, workerProducts);
}

async function getProducts() {
  return await axios.get("/data/products.json");
}
function* workerProducts(): any {
  try {
    const response = yield call(getProducts);
    yield put(productsSlice.actions.getProductsSuccess(response.data));
  } catch (error) {
    yield put(productsSlice.actions.getProductsFailure());
    console.log(error);
  }
}
