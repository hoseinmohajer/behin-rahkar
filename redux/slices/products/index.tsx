import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface DataInterface {
  id: number;
  title: string;
  price: number;
  description: string;
  category: string;
  image: string;
  rating: {
    rate: number;
    count: number;
  };
}
interface InitialStateInterface {
  products: {
    loading: boolean;
    data: DataInterface[];
  };
}
const initialState: InitialStateInterface = {
  products: {
    loading: false,
    data: [],
  },
};
export const productsSlice = createSlice({
  name: "products",
  initialState,
  reducers: {
    getProducts: () => {
      // some code here
    },
    getProductsSuccess: (state, { payload }: PayloadAction<DataInterface[]>) => ({
      ...initialState,
      products: { loading: false, data: payload },
    }),
    getProductsFailure: () => {},
  },
});
export type ProductsSlice = typeof productsSlice;
export const { getProducts, getProductsSuccess, getProductsFailure } = productsSlice.actions;
