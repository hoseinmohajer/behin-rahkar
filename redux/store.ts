import { configureStore, getDefaultMiddleware, combineReducers } from "@reduxjs/toolkit";
import { productsSlice } from "./slices/products";

import createSagaMiddleware from "@redux-saga/core";
import rootSaga from "./sagas";

const sagaMiddleware = createSagaMiddleware();

const rootReducer = combineReducers({
  products: productsSlice.reducer,
});
export type RootState = ReturnType<typeof rootReducer>;

const middleware = [...getDefaultMiddleware({ thunk: false }), sagaMiddleware];
const store = configureStore({
  reducer: rootReducer,
  middleware,
  devTools: process.env.NODE_ENV !== "production",
});
sagaMiddleware.run(rootSaga);

export default store;
